const handleError = () => {
    return null
}

export const getCurrentLocation = (setLocation) => {
    if (!navigator.geolocation) {
        return null;
    } else {
        navigator.geolocation.getCurrentPosition(setLocation, handleError);
    }
}