import React, { useState } from "react";

import "./AdminPage.scss";

const Admin = () => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [isAdmin, setIsAdmin] = useState(false);

  const onClickLogin = () => {
    if (username === "wanconsult" && password === "wanconsult") setIsAdmin(true);
  }

  const renderLogin = () => (
    <div className="admin-login">
      <h1>Admin</h1>
      <form className="form" onSubmit={onClickLogin} >
        <input type="text" placeholder="Username" onChange={e => setUsername(e.target.value)} />
        <input type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} />
        <input type="submit" id="login-button" value="Login"/>
      </form>
    </div>
  );

  const renderAdmin = () => (
    <div className="admin-detail">
      <h1>Coming Soon...</h1>
    </div>
  )

  return (
    <div className="admin">
      {!isAdmin && renderLogin()}
      {isAdmin && renderAdmin()}
    </div>
  )
}

export default Admin;