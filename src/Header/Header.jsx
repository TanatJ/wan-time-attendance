import "./Header.scss";

import React from "react";

const Header = () => {
  return (
    <div className="header">
      <nav>
        <input type="checkbox" id="nav" className="hidden" />
        <label htmlFor="nav" className="nav-btn">
          <i></i>
          <i></i>
        </label>
        <div className="logo">
          <a href="/">
            <img src="/static/icon/wan-logo.jpg" alt="wan-consultant" height="50" />
          </a>
        </div>
        <div className="header-wrapper">
          <ul>
            <li className="nav-item"><a href="/">Home</a></li>
            <li className="nav-item"><a href="/admin">Admin</a></li>
            <li className="nav-item"><a href="/signup">Sign up</a></li>
          </ul>
        </div>
      </nav>
    </div>
  )
}

export default Header;