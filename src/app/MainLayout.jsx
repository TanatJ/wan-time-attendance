import React from "react";

import { Header } from "../Header";

import "./MainLayout.scss"

const MainLayout = props => {
  return (
    <div className="main-layout">
      <Header />
      {props.children}
    </div>
  )
}

export default MainLayout;