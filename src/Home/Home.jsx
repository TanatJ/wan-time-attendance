import "./Home.scss";

import React, { useState, useEffect } from "react";
import axios from "axios";

import { Profile, Login } from "../Profile";

const Home = () => {
  const [isLogin, setIsLogin] = useState(false);
  const [user, setUser] = useState({});


  const getToken = async localToken => {
    const response = await axios.get(`api/get-token?token=${localToken}`);

    if (response.status === 200) {
      const {token, firstname, lastname, id } = response.data;
      
      if (response.data.isLogin) {
        setUser({
          firstname,
          lastname,
          token,
          docId: id
        });
        setIsLogin(response.data.isLogin);
      }
    }
  }
  useEffect(() => {
    const localToken = localStorage.getItem("token");
    if (localToken) getToken(localToken);
  }, [])
  
  return (
    <div className="home">
      {!isLogin && 
        <Login 
          setIsLogin={setIsLogin}
          setUser={setUser}
        />
      }
      {isLogin && 
        <Profile
          user={user}
          setIsLogin={setIsLogin}
        />
      }
    </div>
  )
}

export default Home;