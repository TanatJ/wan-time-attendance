import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import moment from "moment";

import { getCurrentLocation } from "../../services/location";

import ProfileApiService from "./ProfileApiService";

import "./Profile.scss";

const Profile = props => {
  const { user, setIsLogin } = props;
  
  const [userRecord, setUserRecord] = useState([]);
  const [currentLocation, setCurrentLocation] = useState({});
  const [isRecord, setIsRecord] = useState();

  const fetchSchedule = async (token, id) => {
    const response = await axios.post("/api/get-record", {
      token,
      id
    });

    // console.warn("fetch Schedule", response);

    if (response.status === 200 && response.data.length > 0) {
     setUserRecord(response.data);
     setIsRecord(isCheckIn(response.data[0]));
    }
  }

  const isCheckIn = record => {
    if (record.type === "check-in") return "check-out";
    return "check-in";
  }

  const setPosition = position => {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    setCurrentLocation({
      latitude: latitude,
      longitude: longitude
    });
  }

  const onClickRecordTime = () => {
    const profileApi = new ProfileApiService();

    const checkType = userRecord.length > 0 && isCheckIn(userRecord[0]) || "check-in";

    if (Object.keys(currentLocation).length > 0) {
      const response = profileApi.recordSchedule({
        id: user.docId,
        location: currentLocation && `${currentLocation.latitude}, ${currentLocation.longitude}`,
        type: checkType
      });

      alert("success", response);
      return window.location.reload();
    }
    return alert("please allow location permission");
  }

  const onClickLogOut = () => {
    localStorage.removeItem("token");
    setIsLogin(false);
  }

  useEffect(() => {
    getCurrentLocation(setPosition);
    fetchSchedule(user.token, user.docId);
  }, [isRecord])

  const renderBodyTable = (data) => (
    <tr key={data.timestamp._seconds}>
      <td data-th="Time">{moment.unix(data.timestamp._seconds).format("Y/MMM/D, HH:mm:ss") }</td>
      <td data-th="Location">{data.location}</td>
      <td data-th="Type">{data.type}</td>
    </tr>
  )

  return (
    <div className="profile">
      <div style={{ textAlign: "center" }}>
        <h1>Welcome</h1>
        <h2>{user.firstname} {user.lastname}</h2>
        <input type="button" value="Sing Out" onClick={onClickLogOut} />
        <h4>Today is {moment().format("MMMM Do YYYY")}</h4>
        <input type="button" value={isRecord ? isRecord : "check-in"} onClick={onClickRecordTime} />
      </div>
      <table className="rwd-table">
        <thead>
          <tr>
            <th>Time</th>
            <th>Location</th>
            <th>Type</th>
          </tr>
        </thead>
        <tbody>
          {userRecord.map(data => renderBodyTable(data))}
        </tbody>
      </table>
    </div>
  )
}

Profile.propTypes = {
  user: PropTypes.shape({
    firstname: PropTypes.string,
    lastname: PropTypes.string,
    token: PropTypes.string.isRequired,
    docId: PropTypes.string.isRequired
  }),
  setIsLogin: PropTypes.func.isRequired
};

export default Profile;



