import axios from "axios";

export default class ApiService {
  async fetchSchedule ({token, docId}) {
    const response = await axios.post("/api/get-record", {
      token,
      id: docId
    });

    return response.data;
  }

  async recordSchedule ({ id, location, timestamp, type }) {
      await axios.post("/api/post-record", {
          id, location, timestamp, type
      });
  }

  async signUp (user) {
    const response = await axios.post("/api/register", {
      user
    });

    return response.data;
  }

  async login (username, password, channel = "WEB") {
    const response = await axios.post("/api/login", {
      username,
      password,
      channel
    });

    return response.data;
  }
};
