import React, { useState } from "react";
import Router from 'next/router'

import ProfileApiService from "./ProfileApiService";

import "./SignUp.scss";

const SignUp = () => {
  const [user, setUser] = useState({})

  const onSubmitRegister = async e => {
    e.preventDefault();
    
    const profileServiceApi = new ProfileApiService();

    const response = await profileServiceApi.register(user);

    if (response.code !== 200) return alert(response.message);

    alert(response.message);
    Router.push("/");
  }

  return (
    <div className="register">
      <div className="container">
        <h1>Sign Up</h1>
        <form onSubmit={onSubmitRegister}>
          <input type="text" placeholder="Username" onChange={e => setUser({ ...user, username: e.target.value })} required />
          <input type="password" placeholder="Password" onChange={e => setUser({ ...user, password: e.target.value })} required />
          <input type="text" placeholder="Firstname" onChange={e => setUser({ ...user, firstname: e.target.value })} required />
          <input type="text" placeholder="Lastname" onChange={e => setUser({ ...user, lastname: e.target.value })} required />
          <input type="email" placeholder="Email" onChange={e => setUser({ ...user, email: e.target.value })} required />
          <input type="text" placeholder="Phone Number" onChange={e => setUser({ ...user, phoneNumber: e.target.value })} required />
          <input type="submit" value="Sign Up" />
        </form>
      </div>
    </div>
  )
}

export default SignUp;
