import React, { useState } from "react";
import Link from "next/link";

import ProfileApiService from "./ProfileApiService";

import "./Login.scss";

const Login = props => {
  const { setIsLogin, setUser } = props

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const onClickLogin = async e => {
    e.preventDefault();

    const profileApiService = new ProfileApiService();

    const response = await profileApiService.login(username, password, "WEB");

    const { isLogin, firstname, lastname, token, id } = response;

    if (isLogin) {
      localStorage.setItem("token", token);

      setUser({
        firstname,
        lastname,
        token,
        docId: id
      });

      setIsLogin(isLogin);
    }
  }

  return (
    <div className="login">
      <div className="container">
        <h1>Welcome</h1>
        <form className="form" onSubmit={onClickLogin} >
          <input type="text" placeholder="Username" onChange={e => setUsername(e.target.value)} />
          <input type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} />
          <input type="submit" id="login-button" value="Login"/>
        </form>
      </div>
    </div>
  )
}

export default Login;