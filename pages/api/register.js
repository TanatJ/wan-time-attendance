import { db, timeStamp } from "../../services/data/firebase";

export default async function (req, res) {
  if (req.method === "POST") {
    const { user } = req.body;

    if (!user) return null;

    const concatUser = user.username + user.password;
    const binaryUser = Buffer.from(concatUser, "utf8");
    const generateToken = binaryUser.toString("base64");
    
    try {
      const firebaseUser = db.collection("users");
      const usernameResponse = await firebaseUser
        .where("username", "==", user.username)
        .get();

        if (!usernameResponse.empty) 
          return res.status(200)
            .send({
              code: 401,
              message: `Username is already use`
            });

        const registerResponse = await firebaseUser.add({
          firstname: user.firstname,
          lastname: user.lastname,
          username: user.username,
          password: user.password,
          email: user.email,
          phoneNumber: user.phoneNumber,
          token: generateToken,
          created: timeStamp,
          deviceId: null,
          lastLogin: null
        });

        if (!registerResponse)
          return res.status(200)
            .send({
              code: 404,
              message: `Register not Success`
            });

        return res.status(200).send({
          code: 200,
          message: `Register Success`,
          token: generateToken
        });
    } catch (error) {
      res.status(400).send(`Service Register is invalid : `, error);
      throw error;
    }
  }
}