import { db } from "../../services/data/firebase";

export default async (req, res) => {
  if (req.method === "POST") {
    const { username, password, channel, deviceId } = req.body;

    const concatUser = username + password;
    const binaryUser = Buffer.from(concatUser, "utf8");
    const generateToken = binaryUser.toString("base64");
    
    let isChecking = false;
    let docData = null;
    let docId = null;
    
    const user = db.collection("users");
    await user.where("username", "==", username)
      .where("password", "==", password)
      .get()
      .then(snapshot => {
        if (snapshot.empty) 
          return res.status(200)
            .send({
              code: 404,
              message: "user information is invalid"
            });

        snapshot.forEach(doc => {
          if (!doc.data().deviceId && channel !== "WEB") {
            user.doc(doc.id).update({deviceId: deviceId})
          }

          user.doc(doc.id).update({token: generateToken});
          docId = doc.id;
          isChecking = doc.data() ? true : false;
          docData = doc.data();
        })
      }).catch(err => {
        res.status(400).send(`Service Login is invalid : `, error);
        throw err;
      });
    if (docData.deviceId && (docData.deviceId !== deviceId) && channel !== "WEB")
      return res.status(200).json({
        code: 401,
        message: "user device id is invalid."
      });

    const response = {
      code: !isChecking ? 404 : 200,
      message: !isChecking ? "invalid" : "success",
      token: !isChecking ? null : generateToken,
      id: !isChecking ? null : docId,
      firstname: !isChecking ? null : docData.firstname,
      lastname: !isChecking ? null : docData.lastname,
      isLogin: !isChecking ? false : true
    };

    return res.status(200).json(response);
  }
}