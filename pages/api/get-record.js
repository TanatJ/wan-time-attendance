import { db } from "../../services/data/firebase";

export default async (req, res) => {    
  if (req.method === "POST") {
    const { token, id } = req.body

    let data = [];
    const user = db.collection("users");
    await user.doc(id)
      .collection("schedules")
      .orderBy("timestamp", "desc")
      .get()
        .then(snapshot => {
          if (snapshot.empty)
            return res.status(200)
              .send({
                code: 401,
                message: `Record not found`
              });

          snapshot.forEach(doc => {
            data.push(doc.data());
          });
        }).catch(error => {
          res.status(400)
            .send(`Service get record is invalid : `, error);
          throw err;
        });

    res.status(200).json(data);
  }
}