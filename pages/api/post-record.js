import { db, timeStamp } from "../../services/data/firebase";

export default async(req, res) => {
    if (req.method === "POST") {
        const { id, location, type } = req.body;

        if (!id || !location || !type) return res.status(404).send(`Service Record cannot process some parameter not found.`);
        try {
            const user = db.collection("users");
            const response = await user.doc(id).collection("schedules").add({
                location,
                timestamp: timeStamp,
                type
            })

            const rawItem = response;

            if (rawItem) res.status(201).send(`Service Record Success`);
        } catch (error) {
            res.status(400).send(`Service Record is invalid : `, error);
            throw error;
        }
    }
}