import { db } from "../../services/data/firebase";

export default async (req, res) => {    
  if (req.method === "GET") {
    const { token } = req.query;

    let isChecking = false;
    let docId;

    const user = db.collection("users");
    await user.where("token", "==", token).get()
      .then(snapshot => {
        if (snapshot.empty) return console.log("no matching data");

        snapshot.forEach(doc => {
          console.log(doc.id, "=>", doc.data());
          docId = doc.id;
          isChecking = doc.data();
        })
      }).catch(err => {
        console.log("Error getting documents", err);
        throw err;
      });

    const response = {
      token: isChecking.token,
      id: isChecking && docId,
      firstname: isChecking.firstname,
      lastname: isChecking.lastname,
      isLogin: !isChecking ? false : true
    }

    res.json(response);
  }
}