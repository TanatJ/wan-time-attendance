import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <link rel="shortcut icon" type="image/x-icon" href="/static/icon/wan-logo.jpg" size="16x16" />
          <link rel="shortcut icon" type="image/png" href="/static/icon/wan-logo.jpg" size="32x32" />
          <link rel="shortcut icon" type="image/png" href="/static/icon/wan-logo.jpg" size="180x180" />
          <link rel="apple-touch-icon" type="image/png" href="/static/icon/wan-logo.jpg" size="180x180" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument