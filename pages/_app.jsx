import App from 'next/app'
import Head from 'next/head'
import React from 'react'

import { MainLayout } from "../src/app";
export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <>
        <Head>
          <title>Wan Consult</title>
        </Head>
        <MainLayout>
          <Component {...pageProps} />
        </MainLayout>
      </>
    )
  }
};
